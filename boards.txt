# Copyright (c) 2015-2016 M2M4ALL BV.  All right reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# ResPy ONE
# ---------------------------------------
respy_one.name=ResPy ONE
respy_one.vid.0=0x2341
respy_one.pid.0=0x804d
respy_one.vid.1=0x2341
respy_one.pid.1=0x004d
respy_one.upload.tool=bossac
respy_one.upload.protocol=sam-ba
respy_one.upload.maximum_size=262144
respy_one.upload.use_1200bps_touch=true
respy_one.upload.wait_for_upload_port=true
respy_one.upload.native_usb=true
respy_one.build.mcu=cortex-m0plus
respy_one.build.f_cpu=48000000L
respy_one.build.usb_product="ResPy ONE"
respy_one.build.usb_manufacturer="Innovatica"
respy_one.build.board=RESPY_ONE
respy_one.build.core=arduino
respy_one.build.extra_flags=-D__SAMD21J18A__ {build.usb_flags}
respy_one.build.ldscript=linker_scripts/gcc/flash_with_bootloader.ld
respy_one.build.openocdscript=openocd_scripts/respy_one.cfg
respy_one.build.variant=respy_one
respy_one.build.variant_system_lib=
respy_one.build.vid=0x2341
respy_one.build.pid=0x804d
respy_one.bootloader.tool=openocd
respy_one.bootloader.file=zero/samd21_sam_ba_respy.bin